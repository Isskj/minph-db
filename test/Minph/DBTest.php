<?php

use PHPUnit\Framework\TestCase;
use Minph\DB;

class DBTest extends TestCase
{
    public function testAppBoot()
    {
        $db = new DB(
            'mysql:host=127.0.0.1;port=3308;dbname=test_db',
            'root',
            'root'
        );
        $res = $db->queryOne('SELECT * FROM user ORDER BY age');
        $this->assertEquals($res->name, 'test user A');
        $this->assertEquals($res->age, 10);
    }
}
